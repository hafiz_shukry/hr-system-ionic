import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../model/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: User;
  registerForm : FormGroup;

  constructor(
    private router : Router,
    private formBuilder: FormBuilder,
    private _user: UserService

  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname : ['',[Validators.required]],
      lastname : ['',[Validators.required]],
      email : ['',[Validators.required]],
      password : ['',[Validators.required]],
      confirmPassword : ['',[Validators.required]],
    })
  }


  onSubmit(){
    let user : User = new User();
    user.setFirstname(this.registerForm.controls['firstname'].value);
    user.setLastname(this.registerForm.controls['lastname'].value);
    user.setEmail(this.registerForm.controls['email'].value);
    user.setPassword(this.registerForm.controls['password'].value);

    console.log("User Registeration : " , user);
    
    this._user.userRegister(user).subscribe(
      success =>{
        this.router.navigate(['/home'])    
      },
      error => {
        console.log(error);
        
      }

    )
  }

}
