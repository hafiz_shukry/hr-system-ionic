export class User{
    id : string;
    username : string;
    email : string;
    firstname : string;
    lastname : string;
    password : string;
    newPassword : string;

	public setId(id: string) {
		this.id = id;
    }

    public setUsername(username: string) {
		this.username = username;
    }

    public setEmail(email: string) {
		this.email = email;
    }

    public setFirstname(firstname: string) {
		this.firstname = firstname;
    }

    public setLastname(lastname: string) {
		this.lastname = lastname;
    }

    public setPassword(password: string) {
		this.password = password;
    }

    public setNewPassword(newPassword: string) {
		this.newPassword = newPassword;
    }
}