import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../model/user';
import { HttpHeaders, HttpClient } from "@angular/common/http";

@Injectable()
export class UserService{

    //api
    private userUrl = `http://localhost:8080/user`

    constructor(
        private http: HttpClient,
    ){}

    userRegister(user : User): Observable<User>{
        return this.http.post<User>(this.userUrl + '/register', user)
    }
}